"""Placeholder for the term module."""

# ls is used to list the files in the current directory from the terminal
# pwd is used to print the current directory from the notebook
# cd ~ will get you back to the root

# play with black, pylint, and mypy


def my_func(a, b):
    """Return the sum of two numbers.

    Parameters
    ----------
    a : int
        First number.
    b : int
        Second number.

    Returns
    -------
    int
        Sum of a and b.
    """
    return a + b


if __name__ == "__main__":
    print(my_func(2, 3))
